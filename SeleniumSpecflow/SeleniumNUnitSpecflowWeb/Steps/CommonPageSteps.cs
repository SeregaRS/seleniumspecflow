﻿namespace SeleniumNUnitSpecflowWeb.Steps
{
    using System;

    using NUnit.Framework;

    using SeleniumNUnitSpecflowWeb.Core;

    using TechTalk.SpecFlow;

    /// <summary>
    /// Contains common steps
    /// </summary>
    [Binding]
    public class CommonPageSteps : BaseStep
    {
        /// <summary>
        /// Page is loaded 
        /// </summary>
        [When(@"Page is loaded")]
        public void WhenPageIsLoaded()
        {
            BrowserPool.CurrentBrowser.Instance.WaitForPageLoad();
        }

        /// <summary>
        /// Verify page title
        /// </summary>
        /// <param name="pageTitle">expected page title</param>
        [Then(@"Page title should contain ""(.*)"" text")]
        public void ThenPageTitleShouldContainsText(string pageTitle)
        {
            Assert.IsTrue(BrowserPool.CurrentBrowser.Instance.Title.Contains(pageTitle));
        }
    }
}
