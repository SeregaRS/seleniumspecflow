﻿namespace SeleniumNUnitSpecflowWeb.Steps
{
    using System;

    using NUnit.Framework;

    using SeleniumNUnitSpecflowWeb.Core;

    using TechTalk.SpecFlow;

    [Binding]
    public class ToDoMvcAngularJsSteps : BaseStep
    {
        /// <summary>
        /// Given I Have Navigated To The Page
        /// </summary>
        /// <param name="pageUrl"></param>
        [Given(@"I have navigated to the page ""(.*)""")]
        public void GivenIHaveNavigatedToThePage(string pageUrl)
        {
            BrowserPool.CurrentBrowser.Instance.Navigate().GoToUrl(pageUrl);
        }
        
        [Then(@"I should see logo image")]
        public void ThenIShouldSeeLogoImage()
        {
            // Check Element Stub
            Assert.IsFalse(string.IsNullOrEmpty("Stub"));
        }
        
        [Then(@"I should see alternative logo image")]
        public void ThenIShouldSeeAlternativeLogoImage()
        {
            // Check Element Stub
            Assert.IsFalse(string.IsNullOrEmpty("Stub"));
        }
        
        [Then(@"I should see ToDos component")]
        public void ThenIShouldToDosComponent()
        {
            // Check Element Stub
            Assert.IsFalse(string.IsNullOrEmpty("Stub"));
        }
        
        [Then(@"I should see left side pane component")]
        public void ThenIShouldSeeLeftSidePaneComponent()
        {
            // Check Element Stub
            Assert.IsFalse(string.IsNullOrEmpty("Stub"));
        }
    }
}
