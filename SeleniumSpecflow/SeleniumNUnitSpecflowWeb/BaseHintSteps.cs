﻿namespace SeleniumNUnitSpecflowWeb
{
    using System;

    using SeleniumNUnitSpecflowWeb.Core;

    using TechTalk.SpecFlow;

    /// <summary>
    /// Base Hint Steps
    /// </summary>
    [Binding]
    public class BaseHintSteps
    {
        private Browser newBrowser;
        
        /// <summary>
        /// Run method before every scenario
        /// Start new instance of browser for any test 
        /// </summary>
        [BeforeScenario]
        public void BeforeScenario()
        {
            Console.WriteLine("BeforeScenario");
            this.newBrowser = new Browser();
            this.newBrowser.InitBrowserFromFactory();
            BrowserPool.RegisterAndMakeCurrentBrowser("Browser", this.newBrowser);
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Console.WriteLine("AfterScenario");
            this.newBrowser?.Dispose();
        }

        /// <summary>
        /// Before Step
        /// </summary>
        [BeforeStep]
        public void BeforeStep()
        {
            Console.WriteLine("BeforeStep");
        }

        /// <summary>
        /// After Step
        /// </summary>
        [AfterStep]
        public void AfterStep()
        {
            Console.WriteLine("AfterStep");
        }

        /// <summary>
        /// Before Test Run
        /// </summary>
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            Console.WriteLine("BeforeTestRun");
        }

        /// <summary>
        /// Before Feature
        /// </summary>
        [BeforeFeature]
        public static void BeforeFeature()
        {
            Console.WriteLine("BeforeFeature");
        }

        /// <summary>
        /// After Feature
        /// </summary>
        [AfterFeature]
        public static void AfterFeature()
        {
            Console.WriteLine("AfterFeature");
        }

        /// <summary>
        /// After Test Run
        /// </summary>
        [AfterTestRun]
        public static void AfterTestRun()
        {
            Console.WriteLine("AfterTestRun");
        }
    }
}
