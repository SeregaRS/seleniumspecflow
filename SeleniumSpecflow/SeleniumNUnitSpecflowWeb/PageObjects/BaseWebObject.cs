﻿namespace SeleniumNUnitSpecflowWeb.PageObjects
{
    /// <summary>
    /// Base Web Object (custom control)
    /// </summary>
    public class BaseWebObject : IBaseWebObject
    {
        /// <summary>
        /// Base Web Object
        /// </summary>
        protected BaseWebObject()
        {
        }
    }
}
