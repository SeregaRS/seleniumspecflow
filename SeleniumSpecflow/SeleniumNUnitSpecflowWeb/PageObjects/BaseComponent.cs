﻿namespace SeleniumNUnitSpecflowWeb.PageObjects
{
    /// <summary>
    /// Base Component
    /// </summary>
    public class BaseComponent : BaseWebObject, IBaseComponent
    {
        /// <summary>
        /// Base Component
        /// </summary>
        public BaseComponent()
        {
        }
    }
}
