﻿namespace SeleniumNUnitSpecflowWeb.PageObjects.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class TodoMvcHomePage : BasePage
    {
        // Logo
        // Header Image
        // Header Buttons: Download, ViewOnGitHub, Blog
        // Introduction
        // Examples

        private By headerLogoImage = By.ClassName("logo");
        private By headerLogoText = By.CssSelector(".col-md-8>p");

        // ToDo: Header Logo Navigation Menu - Separate Component
        private readonly By headerNavigationButtonsContainer = By.TagName("nav");
        private readonly By headerDownloadNavigationButton = By.XPath("//nav/a[text()='Download']");
        private readonly By headerViewOnGitHubNavigationButton = By.XPath("//nav/a[text()='View on GitHub']");
        private readonly By headerBlogNavigationButton = By.XPath("//nav/a[text()='Blog']");

        /// <summary>
        /// Initializes a new instance of the <see cref="TodoMvcHomePage"/> class. 
        /// </summary>
        public TodoMvcHomePage()
        {
        }
    }
}
