﻿namespace SeleniumNUnitSpecflowWeb.PageObjects.Pages
{
    using SeleniumNUnitSpecflowWeb.PageObjects.Components;
    using SeleniumNUnitSpecflowWeb.PageObjects.Components.LeftSidePane;

    /// <summary>
    /// AngularJS Page
    /// </summary>
    public class AngularJsPage : BasePage
    {
        /// <summary>
        /// Todos Component
        /// </summary>
        public ToDos Todos { get; private set; }

        /// <summary>
        /// LeftSide Pane Component
        /// </summary>
        public LeftSidePane LeftSidePane { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AngularJsPage"/> class. 
        /// </summary>
        public AngularJsPage()
        {
            this.Todos = new ToDos();
            this.LeftSidePane = new LeftSidePane();
        }
    }
}
