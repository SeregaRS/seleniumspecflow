﻿namespace SeleniumNUnitSpecflowWeb.PageObjects.Components
{
    /// <summary>
    /// Header Navigation Component (TodoMvc home page)
    /// </summary>
    public class HeaderNavigationButtons : BaseComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderNavigationButtons"/> class.
        /// </summary>
        public HeaderNavigationButtons()
        {

        }
    }
}
