﻿namespace SeleniumNUnitSpecflowWeb.PageObjects.Components
{
    using OpenQA.Selenium;

    using SeleniumNUnitSpecflowWeb.Core;

    /// <summary>
    /// To Do Item
    /// </summary>
    public class ToDoItem : BaseComponent
    {
        private readonly By stateCheckbox = By.CssSelector("input.toggle.ng-valid");
        private readonly By text = By.CssSelector("");
        private readonly By removeButton = By.CssSelector("");


        /// <summary>
        /// State CheckBox
        /// </summary>
        public IWebElement StateCheckBox { get; private set; }

        /// <summary>
        /// Text
        /// </summary>
        public IWebElement Text { get; private set; }

        /// <summary>
        /// Remove Button
        /// </summary>
        public IWebElement RemoveButton { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToDoItem"/> class. 
        /// </summary>
        /// <param name="containerBy">
        /// </param>
        public ToDoItem(By containerBy)
        {
            this.StateCheckBox = BrowserPool.CurrentBrowser.Instance.GetElement(containerBy).FindElement(this.stateCheckbox);
            this.Text = BrowserPool.CurrentBrowser.Instance.GetElement(containerBy).FindElement(this.text);
            this.StateCheckBox = BrowserPool.CurrentBrowser.Instance.GetElement(containerBy).FindElement(this.removeButton);
        }
    }
}
