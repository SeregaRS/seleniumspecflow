﻿namespace SeleniumNUnitSpecflowWeb.PageObjects.Components
{
    using System;
    using System.Collections.Generic;

    using NUnit.Framework;

    using OpenQA.Selenium;

    using SeleniumNUnitSpecflowWeb.Core;

    /// <summary>
    /// Todos Component
    /// </summary>
    public class ToDos : BaseComponent
    {
        private readonly By componentContainer = By.CssSelector("section.todoapp");
        private readonly By titleText = By.CssSelector("section h1");
        private readonly By inputText = By.CssSelector("input.new-todo");
        private readonly By taskSection = By.CssSelector("section.main");

        private readonly By todoItemsList = By.CssSelector("");
        private readonly By itemsCounter = By.CssSelector("");

        // ToDo Possible new component is required (ToDosFooterStateMenu)
        private readonly By todosAllButton = By.CssSelector("");
        private readonly By todosActiveButton = By.CssSelector("");
        private readonly By todosCompletedButton = By.CssSelector("");

        /// <summary>
        /// Add New To Do Item
        /// </summary>
        /// <param name="itemText"></param>
        /// <returns></returns>
        public ToDos AddNewToDoItem(string itemText)
        {
            return this;
        }

        /// <summary>
        /// Get All To Do Items
        /// </summary>
        /// <param name="itemText"></param>
        /// <returns></returns>
        public List<ToDoItem> GetToDoItems(string itemText)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get To Do Item By Text
        /// </summary>
        /// <param name="itemText"></param>
        /// <returns></returns>
        public ToDoItem GetToDoItemByText(string itemText)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get To Do Item By Number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public ToDoItem GetToDoItemByNumber(int number)
        {
            throw new NotImplementedException();
        }

        private IReadOnlyCollection<IWebElement> GetToDoItemsContaners()
        {
            throw new NotImplementedException();
        }
    }
}
