﻿namespace SeleniumNUnitSpecflowWeb.PageObjects
{
    /// <summary>
    /// Base Page class
    /// </summary>
    public abstract class BasePage : BaseComponent, IBasePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BasePage"/> class.
        /// </summary>
        protected BasePage()
        {
        }
    }
}
