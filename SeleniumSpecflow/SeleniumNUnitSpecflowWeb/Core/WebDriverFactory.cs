﻿namespace SeleniumNUnitSpecflowWeb.Core
{
    using System;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.IE;

    /// <summary>
    /// Web Driver Factory
    /// - Supported browsers
    /// - Settings  
    /// - Session info
    /// </summary>
    public static class WebDriverFactory
    {
        /// <summary>
        /// Create new Web Driver object
        /// </summary>
        /// <param name="session">session parameters</param>
        /// <returns>new WEbDriver object of specific Browser type</returns>
        public static IWebDriver Create(SessionManager session)
        {
            var driver = CreateDriver(session);
            Manage(driver, session);
            return driver;
        }

        /// <summary>
        /// Create specific Web driver instance
        /// </summary>
        /// <param name="session"></param>
        /// <returns> Web driver type</returns>
        private static IWebDriver CreateDriver(SessionManager session)
        {
            switch (session.WebDriverType)
            {
                case WebDriverType.Chrome:
                    return Chrome(session);
                case WebDriverType.Firefox:
                    return Firefox(session);
                case WebDriverType.InternetExplorer:
                    return InternetExplorer(session);
                default:
                    return Firefox(session);
            }
        }

        private static IWebDriver Firefox(SessionManager session)
        {
            var firefoxOptions = new FirefoxOptions();
            firefoxOptions.Profile.AcceptUntrustedCertificates = session.AcceptUntrustedCertificates;
            firefoxOptions.SetPreference("browser.startup.homepage", "about:blank");
            return new FirefoxDriver(firefoxOptions);
        }

        private static IWebDriver Chrome(SessionManager session)
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = session.HideCommandPromptWindow;
            return new ChromeDriver(service, new ChromeOptions());
        }

        private static IWebDriver InternetExplorer(SessionManager session)
        {
            var service = InternetExplorerDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = session.HideCommandPromptWindow;
            return new InternetExplorerDriver(service, new InternetExplorerOptions());
        }

        private static void Manage(IWebDriver driver, SessionManager session)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(session.WaitForElementTimeout);
            driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(session.ScriptTimeout);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(session.PageLoadTimeout);
        }
    }
}
