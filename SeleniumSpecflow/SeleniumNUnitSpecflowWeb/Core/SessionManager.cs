﻿namespace SeleniumNUnitSpecflowWeb.Core
{
    /// <summary>
    /// Session Manager
    /// ToDo - Currently not used. Possible to be used in scope Test Context
    /// </summary>
    public class SessionManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionManager"/> class.
        /// </summary>
        public SessionManager()
        {
            var testSettings = new TestSettings();
            this.WaitForElementTimeout = testSettings.ElementLoadTimeout;
            this.PageLoadTimeout = testSettings.PageLoadTimeout;
            this.ScriptTimeout = testSettings.JavaScriptTimeout;
            this.WebDriverType = testSettings.WebDriverType;
        }

        /// <summary>
        /// AcceptUntrustedCertificates (chrome)
        /// </summary>
        public bool AcceptUntrustedCertificates { get; set; }

        /// <summary>
        /// HideCommandPromptWindow
        /// </summary>
        public bool HideCommandPromptWindow { get; set; }

        /// <summary>
        /// ImplicitlyWait
        /// </summary>
        public int WaitForElementTimeout { get; set; }

        /// <summary>
        /// PageLoadTimeout
        /// </summary>
        public int PageLoadTimeout { get; set; }

        /// <summary>
        /// ScriptTimeout
        /// </summary>
        public int ScriptTimeout { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WebDriverType WebDriverType { get; set; }
    }
}
