﻿namespace SeleniumNUnitSpecflowWeb.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NUnit.Framework;

    /// <summary>
    /// Browser Pool
    /// This class used for access point to the current test run browser instance
    /// Might be extended to use several browsers
    /// </summary>
    public class BrowserPool
    {
        private static readonly Dictionary<string, Browser> BrowserList = new Dictionary<string, Browser>();

        private static string currentBrowserName;

        /// <summary>
        /// Access point for current instance of Browser
        /// </summary>
        public static Browser CurrentBrowser
        {
            get
            {
                string browserName = CurrentBrowserName;

                if (string.IsNullOrWhiteSpace(browserName))
                {
                    throw new InvalidOperationException("No browser exists to be used as current.");
                }

                return BrowserList[browserName];
            }
        }

        /// <summary>
        /// Browser Name
        /// </summary>
        public static string CurrentBrowserName
        {
            get
            {
                ActualiseCurrentBrowserName();
                return currentBrowserName;
            }
        }

        /// <summary>
        /// Register And Make Current Browser
        /// </summary>
        /// <param name="browserName">The reference name of a browser.</param>
        /// <param name="newBrowser">The Browser object to register in the pool.</param>
        public static void RegisterAndMakeCurrentBrowser(string browserName, Browser newBrowser)
        {
            RegisterNewBrowser(browserName, newBrowser);
            MakeCurrentBrowser(browserName);
        }

        /// <summary>
        /// Add new Browser to the pool
        /// </summary>
        /// <param name="browserName"></param>
        /// <param name="newBrowser"></param>
        private static void RegisterNewBrowser(string browserName, Browser newBrowser)
        {
            Assert.IsNotNull(newBrowser);
            browserName = browserName.Trim();
            RemoveDisposedInstances();
            if (!BrowserList.ContainsKey(browserName))
            {
                BrowserList.Add(browserName, newBrowser);
            }

            ActualiseCurrentBrowserName();
        }

        /// <summary>
        /// Switching the current browser.
        /// </summary>
        /// <param name="browserName">The reference name of a browser.</param>
        private static void MakeCurrentBrowser(string browserName)
        {
            BrowserPool.RemoveDisposedInstances();
            browserName = browserName.Trim();

            if (BrowserList.ContainsKey(browserName))
            {
                currentBrowserName = browserName;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    nameof(browserName),
                    $"No browser with reference name '{browserName}' is available");
            }
        }

        /// <summary>
        /// Actualize Current Browser Name
        /// </summary>
        private static void ActualiseCurrentBrowserName()
        {
            if (string.IsNullOrWhiteSpace(currentBrowserName) || !BrowserList.ContainsKey(currentBrowserName))
            {
                currentBrowserName = string.Empty;
            }

            if (string.IsNullOrWhiteSpace(currentBrowserName) && BrowserList.Any())
            {
                currentBrowserName = BrowserList.Keys.First();
            }
        }

        /// <summary>
        /// Remove Disposed Driver Instances
        /// </summary>
        private static void RemoveDisposedInstances()
        {
            string[] keys = BrowserList.Where(pair => pair.Value.IsDisposed).Select(pair => pair.Key).ToArray();

            foreach (string key in keys)
            {
                BrowserList.Remove(key);
            }
        }
    }
}
