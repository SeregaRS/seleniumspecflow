﻿namespace SeleniumNUnitSpecflowWeb.Core
{
    using System;
    using System.Linq;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.Extensions;
    using OpenQA.Selenium.Support.UI;

    using SeleniumNUnitSpecflowWeb.PageObjects;
    using SeleniumNUnitSpecflowWeb.PageObjects.Pages;

    /// <summary>
    /// Additional Extension method to work with Web Driver
    /// </summary>
    public static class WebDriverCustomExtensions
    {
        /// <summary>
        /// Click Link By Text
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="driver">
        /// The driver.
        /// </param>
        /// <param name="linkText">
        /// </param>
        /// <returns>
        /// page 
        /// </returns>
        public static T ClickLinkByText<T>(this IWebDriver driver, string linkText) where T : BasePage
        {
            driver.FindElement(By.LinkText(linkText)).Click();
            return Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Get Element
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="elementLocator"></param>
        /// <returns> element or null </returns>
        public static IWebElement GetElement(this IWebDriver driver, By elementLocator)
        {
            var elements = driver.FindElements(elementLocator);
            return elements.Any() ? elements[0] : null;
        }

        /// <summary>
        /// Try get Element
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="elementLocator"></param>
        /// <returns> element or null </returns>
        public static IWebElement TryGetElement(this IWebDriver driver, By elementLocator)
        {
            try
            {
                return driver.FindElement(elementLocator);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Verify if element is present in the page DOM model
        /// </summary>
        /// <param name="elementLocator"> locator </param>
        /// <returns> check result </returns>
        public static bool IsElementPresent(this IWebDriver driver, By elementLocator)
        {
            return driver.FindElements(elementLocator).Count > 0;
        }


        /// <summary>
        /// Verify if element displayed
        /// </summary>
        /// <param name="elementLocator"> locator </param>
        /// <returns> check result </returns>
        public static bool IsElementDisplayed(this IWebDriver driver, By elementLocator)
        {
            return driver.FindElement(elementLocator).Displayed;
        }

        /// <summary>
        /// Wait for Ready State = Complete
        /// </summary>
        /// <param name="driver">
        /// The driver.
        /// </param>
        /// <param name="timeoutInSeconds">
        /// The timeout In Seconds.
        /// </param>
        public static void WaitForPageLoad(this IWebDriver driver, int timeoutInSeconds = 10)
        {
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, timeoutInSeconds));
            wait.Until(wd => wd.ExecuteJavaScript<string>("return document.readyState") == "complete");
        }

        /// <summary>
        /// Take Screen (should be used for test failures)
        /// </summary>
        /// <param name="driver">The driver</param>
        /// <param name="screenFolderPath">file path</param>
        /// <param name="screenShotName">file name</param>
        public static void TakeScreenShot(this IWebDriver driver, string screenFolderPath = @"C:\Temp\WebDriver\", string screenShotName = "Screen.png")
        {

            try
            {
                Screenshot screenShot = ((ITakesScreenshot)driver).GetScreenshot();
                screenShot.SaveAsFile(screenFolderPath + screenShotName + DateTime.Now.ToShortTimeString(), ScreenshotImageFormat.Png);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Unable to Take and Save Screenshot");
            }
        }
    }
}
