﻿namespace SeleniumNUnitSpecflowWeb.Core
{
    /// <summary>
    /// Describe available Web Driver Types
    /// </summary>
    public enum WebDriverType
    {
        /// <summary>
        /// Default driver option (Firefox)
        /// </summary>
        Default,

        /// <summary>
        /// Firefox driver option
        /// </summary>
        Firefox,

        /// <summary>
        /// Chrome driver option
        /// </summary>
        Chrome,

        /// <summary>
        /// Internet Explorer driver option
        /// </summary>
        InternetExplorer
    }
}