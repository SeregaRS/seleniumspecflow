﻿namespace SeleniumNUnitSpecflowWeb.Core
{
    using System;

    using OpenQA.Selenium;

    /// <summary>
    /// Selenium Web Driver
    /// ToDo
    /// 1. Add possibility to switch browser type using parameter or configuration - Done
    /// 2. Add manipulation for Driver settings - Done
    /// 3. 
    /// </summary>
    public sealed class Browser : IDisposable
    {
        readonly SessionManager sessionTestSettings = new SessionManager();

        /// <summary>
        /// The current window handle.
        /// </summary>
        public string CurrentWindowHandle => this.Instance.CurrentWindowHandle;

        /// <summary>
        /// Browser title
        /// </summary>
        public string Title => this.Instance.Title;

        /// <summary>
        /// URL
        /// </summary>
        public string Url => this.Instance.Url;

        /// <summary>
        /// Get the WebDriver instance
        /// </summary>
        public IWebDriver Instance { get; private set; }

        /// <summary>
        /// Indicates if a browser object can be used.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// InitDriverFromFactory
        /// </summary>
        public void InitBrowserFromFactory()
        {
            this.Instance = WebDriverFactory.Create(this.sessionTestSettings);
            this.IsDisposed = false;
        }

        /// <summary>
        /// Dispose web driver
        /// </summary>
        public void Dispose()
        {
            if (!this.IsDisposed)
            {
                this.Instance.Quit();
                this.Instance.Dispose();
                this.Instance = null;
            }

            this.IsDisposed = true;
        }
    }
}
