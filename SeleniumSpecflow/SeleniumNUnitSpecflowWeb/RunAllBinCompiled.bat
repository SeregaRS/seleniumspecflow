@ECHO OFF

:: 1. Define neccesary paths
@SET TESTDLL_PATH="bin\Debug\SeleniumNUnitSpecflowWeb.dll"
::@ECHO TESTDLL_PATH = %TESTDLL_PATH%
@SET NUNIT_CONSOLE_RUNNER_PATH="..\packages\NUnit.ConsoleRunner.3.8.0\tools\nunit3-console.exe"
::@ECHO NUNIT_CONSOLE_RUNNER_PATH = %NUNIT_CONSOLE_RUNNER_PATH%
@SET SPECFLOW_EXE_PATH="..\packages\SpecFlow.2.4.0\tools\specflow.exe"
::@ECHO SPECFLOW_EXE_PATH = %SPECFLOW_EXE_PATH%
@SET REPORTUNIT_EXE_PATH="..\packages\ReportUnit.1.2.1\tools\ReportUnit.exe"
::@ECHO REPORTUNIT_EXE_PATH = %REPORTUNIT_EXE_PATH%


:: 2. Run tests using NUnit Console
call %NUNIT_CONSOLE_RUNNER_PATH% --labels=All --out=TestResult.txt "--result=TestResult.xml;format=nunit2" %TESTDLL_PATH% 
:: pause

:: 3. Generate Specflow Report
call %SPECFLOW_EXE_PATH% NUnitExecutionReport -p "SeleniumNUnitSpecflowWeb.csproj" 
:: /xsltFile:ReportTransformation.xslt
:: /xmlTestResult:SpecFlowTestResult.xml /testOutput:SpecFlowTestResult.txt /out:SpecFlowTestResult.html
::pause

:: 4. Try to create ReportUnit Report
call %REPORTUNIT_EXE_PATH%  TestResult.xml  ReportUnit.html

::Final step to avoid console closure
pause