﻿Feature: ToDosAngularJSPage
I would like to have info about state of left side pane functionality on the AngularJSPage


@AngularJSPageTest
Scenario: AngularJs Page left side pane quote component text test
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
	Then I should see left side pane component
		And left side pane quoted text should contains 
		"""
		HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop
		"""
		And left side pane quoted AngularJS link should displayed

@AngularJSPageTest
Scenario: AngularJs Page left side pane Official Resources links 
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
	Then I should see left side pane component
		And Official Resources component should displayed
		And Official Resources <OfficialResourcesLink> should displayed
		| OfficialResourcesLink             |
		| Tutorial                          |
		| API Reference                     |
		| Developer Guide                   |
		| Applications built with AngularJS |
		| Blog                              |
		| FAQ                               |
		| Videos                            |

@AngularJSPageTest
Scenario: AngularJs Page left side pane Articles and Guides links 
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
	# Correct valid checks should be added 
	#Then Page title should contain "AngularJS" text

@AngularJSPageTest
Scenario: AngularJs Page left side pane Community links
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
	# Correct valid checks should be added
	#Then Page title should contain "AngularJS" text

@AngularJSPageTest
Scenario: AngularJs Page left side pane Footer text
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
	# Correct valid checks should be added
	#Then Page title should contain "AngularJS" text