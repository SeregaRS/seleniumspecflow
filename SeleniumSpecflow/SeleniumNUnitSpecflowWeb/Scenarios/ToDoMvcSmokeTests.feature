﻿Feature: NavigationSmokeTest
	I would like to have info about current state of the AngularJs loading.
	Moreover I want to have information about state of context on the components

@SmokeTest
Scenario: ToDoMvc Home Page loaded and contains no errors
http://todomvc.com/
	Given I have navigated to the page "http://todomvc.com/"
	When Page is loaded
	Then I should see logo image
		And I should see alternative logo image
		And Page title should contain "TodoMVC" text

@SmokeTest
Scenario: ToDoMvc AngularJs Page loaded and contains no errors 
http://todomvc.com/examples/angularjs/#/
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
	Then I should see ToDos component
		And I should see left side pane component
		And Page title should contain "AngularJS" text

@SmokeTest
Scenario: ToDoMvc React Page loaded and contains no errors 
http://todomvc.com/examples/react/
	Given I have navigated to the page "http://todomvc.com/examples/react/"
	When Page is loaded
	Then I should see ToDos component
		And I should see left side pane component
		And Page title should contain "React" text