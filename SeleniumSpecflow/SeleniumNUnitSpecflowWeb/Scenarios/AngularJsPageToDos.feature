﻿Feature: ToDosAngularJSPage
I would like to have info about state of ToDos component functionality on the AngularJSPage


@AngularJSPageTest
Scenario: AngularJs Page add item to the todos list
	Given I have navigated to the page "http://todomvc.com/examples/angularjs/#/"
	When Page is loaded
		And I input text "ToDoItem" to the todos list
		And I click Enter button
	Then New entry with "ToDoItem" text should displayed in the list
		And new todo item with disabled checkbox should displayed 
		And new todo item with text name "ToDoItem" should displayed 
		And new todo item with remove button should displayed 
		And todos menu should displayed
		And All tab should be enabled by default