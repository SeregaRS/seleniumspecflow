﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable

namespace SeleniumNUnitSpecflowWeb.Scenarios
{
    using TechTalk.SpecFlow;


    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("ToDosAngularJSPage")]
    public partial class AngularJSPageLeftSidePane
    {

        private TechTalk.SpecFlow.ITestRunner testRunner;

#line 1 "AngularJSPageLeftSidePane.feature"
#line hidden

        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo =
                new TechTalk.SpecFlow.FeatureInfo(
                    new System.Globalization.CultureInfo("en-US"),
                    "ToDosAngularJSPage",
                    "I would like to have info about state of left side pane functionality on the Angu" + "larJSPage",
                    ProgrammingLanguage.CSharp,
                    ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }

        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }

        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }

        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }

        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(
                NUnit.Framework.TestContext.CurrentContext);
        }

        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }

        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }

        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("AngularJs Page left side pane quote component text test")]
        [NUnit.Framework.CategoryAttribute("AngularJSPageTest")]
        public virtual void AngularJsPageLeftSidePaneQuoteComponentTextTest()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo =
                new TechTalk.SpecFlow.ScenarioInfo(
                    "AngularJs Page left side pane quote component text test",
                    null,
                    new string[] { "AngularJSPageTest" });
#line 6
            this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 7
            testRunner.Given(
                "I have navigated to the page \"http://todomvc.com/examples/angularjs/#/\"",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Given ");
#line 8
            testRunner.When("Page is loaded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 9
            testRunner.Then(
                "I should see left side pane component",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Then ");
#line hidden
#line 10
            testRunner.And(
                "left side pane quoted text should contains",
                @"HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop",
                ((TechTalk.SpecFlow.Table)(null)),
                "And ");
#line 14
            testRunner.And(
                "left side pane quoted AngularJS link should displayed",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "And ");
#line hidden
            this.ScenarioCleanup();
        }

        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("AngularJs Page left side pane Official Resources links")]
        [NUnit.Framework.CategoryAttribute("AngularJSPageTest")]
        public virtual void AngularJsPageLeftSidePaneOfficialResourcesLinks()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo =
                new TechTalk.SpecFlow.ScenarioInfo(
                    "AngularJs Page left side pane Official Resources links",
                    null,
                    new string[] { "AngularJSPageTest" });
#line 17
            this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 18
            testRunner.Given(
                "I have navigated to the page \"http://todomvc.com/examples/angularjs/#/\"",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Given ");
#line 19
            testRunner.When("Page is loaded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 20
            testRunner.Then(
                "I should see left side pane component",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Then ");
#line 21
            testRunner.And(
                "Official Resources component should displayed",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "And ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] { "OfficialResourcesLink" });
            table1.AddRow(new string[] { "Tutorial" });
            table1.AddRow(new string[] { "API Reference" });
            table1.AddRow(new string[] { "Developer Guide" });
            table1.AddRow(new string[] { "Applications built with AngularJS" });
            table1.AddRow(new string[] { "Blog" });
            table1.AddRow(new string[] { "FAQ" });
            table1.AddRow(new string[] { "Videos" });
#line 22
            testRunner.And(
                "Official Resources <OfficialResourcesLink> should displayed",
                ((string)(null)),
                table1,
                "And ");
#line hidden
            this.ScenarioCleanup();
        }

        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("AngularJs Page left side pane Articles and Guides links")]
        [NUnit.Framework.CategoryAttribute("AngularJSPageTest")]
        public virtual void AngularJsPageLeftSidePaneArticlesAndGuidesLinks()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo =
                new TechTalk.SpecFlow.ScenarioInfo(
                    "AngularJs Page left side pane Articles and Guides links",
                    null,
                    new string[] { "AngularJSPageTest" });
#line 33
            this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 34
            testRunner.Given(
                "I have navigated to the page \"http://todomvc.com/examples/angularjs/#/\"",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Given ");
#line 35
            testRunner.When("Page is loaded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            this.ScenarioCleanup();
        }

        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("AngularJs Page left side pane Community links")]
        [NUnit.Framework.CategoryAttribute("AngularJSPageTest")]
        public virtual void AngularJsPageLeftSidePaneCommunityLinks()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo =
                new TechTalk.SpecFlow.ScenarioInfo(
                    "AngularJs Page left side pane Community links",
                    null,
                    new string[] { "AngularJSPageTest" });
#line 40
            this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 41
            testRunner.Given(
                "I have navigated to the page \"http://todomvc.com/examples/angularjs/#/\"",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Given ");
#line 42
            testRunner.When("Page is loaded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            this.ScenarioCleanup();
        }

        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("AngularJs Page left side pane Footer text")]
        [NUnit.Framework.CategoryAttribute("AngularJSPageTest")]
        public virtual void AngularJsPageLeftSidePaneFooterText()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo =
                new TechTalk.SpecFlow.ScenarioInfo(
                    "AngularJs Page left side pane Footer text",
                    null,
                    new string[] { "AngularJSPageTest" });
#line 47
            this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 48
            testRunner.Given(
                "I have navigated to the page \"http://todomvc.com/examples/angularjs/#/\"",
                ((string)(null)),
                ((TechTalk.SpecFlow.Table)(null)),
                "Given ");
#line 49
            testRunner.When("Page is loaded", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}

#pragma warning restore
#endregion
