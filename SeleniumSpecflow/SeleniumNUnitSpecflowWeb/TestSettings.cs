﻿namespace SeleniumNUnitSpecflowWeb
{
    using System.Configuration;

    using SeleniumNUnitSpecflowWeb.Core;

    /// <summary>
    /// Test Setting from the configuration file
    /// ToDo Look to the way how to make this more expendable. 
    /// ToDO Currently it is quite so difficult to apply new configuration option
    /// ToDo Possible to be converted to the Session (Web Driver) Options
    /// </summary>
    public class TestSettings
    {
        private const string BrowserConfigOptionName = "Browser";
        private const string PageLoadTimeoutConfigOptionName = "PageLoadTimeout";
        private const string WaitForElementTimeoutConfigOptionName = "ElementLoadTimeout";
        private const string WaitForJavaScriptTimeoutConfigOptionName = "JavaScriptTimeout";
        private const string ResultEmailConfigOptionName = "ResultEmail";

        /// <summary>
        /// Web Driver Type
        /// </summary>
        public WebDriverType WebDriverType { get; private set; }

        public int PageLoadTimeout { get; private set; }

        public int ElementLoadTimeout { get; private set; }

        public int JavaScriptTimeout { get; private set; }

        public string ResultEmail { get; private set; } = ConfigurationManager.AppSettings[ResultEmailConfigOptionName];

        /// <summary>
        /// Test Settings
        /// </summary>
        public TestSettings()
        {
            this.ReadTestSettingsFromConfigFile();
        }

        /// <summary>
        /// Read Test Settings From Configuration File
        /// </summary>
        private void ReadTestSettingsFromConfigFile()
        {
            int parsedTimeoutResult;
            this.WebDriverType = this.ResolveWebDriverType(ConfigurationManager.AppSettings[BrowserConfigOptionName]);

            this.PageLoadTimeout = int.TryParse(
                ConfigurationManager.AppSettings[PageLoadTimeoutConfigOptionName], 
                    out parsedTimeoutResult)
                        ? parsedTimeoutResult
                        : 30;

            this.ElementLoadTimeout = int.TryParse(
                ConfigurationManager.AppSettings[WaitForElementTimeoutConfigOptionName],
                    out parsedTimeoutResult)
                        ? parsedTimeoutResult
                        : 30;

            this.JavaScriptTimeout = int.TryParse(
                ConfigurationManager.AppSettings[WaitForJavaScriptTimeoutConfigOptionName],
                    out parsedTimeoutResult)
                        ? parsedTimeoutResult
                        : 30;
        }

        /// <summary>
        /// Resolve Web Driver Type
        /// </summary>
        /// <param name="browserTypeFromConfig">configuration option</param>
        /// <returns> resolved type </returns>
        private WebDriverType ResolveWebDriverType(string browserTypeFromConfig)
        {
            switch (browserTypeFromConfig)
            {
                case "FF":
                    return WebDriverType.Firefox;
                case "CH":
                    return WebDriverType.Chrome;
                case "IE":
                    return WebDriverType.InternetExplorer;
                default:
                    return WebDriverType.Firefox;
            }
        }
    }
}
