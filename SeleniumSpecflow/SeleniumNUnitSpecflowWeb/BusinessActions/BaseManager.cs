﻿namespace SeleniumNUnitSpecflowWeb.BusinessActions
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseManager : IManager
    {
        /// <summary>
        /// Manager Name
        /// </summary>
        public virtual string Name => "DefaultManagerName";
    }
}
