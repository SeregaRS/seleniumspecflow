﻿namespace SeleniumNUnitSpecflowWeb.BusinessActions
{
    using System;
    using System.Collections.Generic;

    using SeleniumNUnitSpecflowWeb.Core;
    using SeleniumNUnitSpecflowWeb.PageObjects;
    using SeleniumNUnitSpecflowWeb.PageObjects.Pages;

    /// <summary>
    /// Navigation Manager
    /// </summary>
    public class NavigationManager : BaseManager
    {
        /// <summary>
        /// Page and url map dictionary
        /// </summary>
        private readonly Dictionary<Type, string> pageUrlMap = new Dictionary<Type, string>
                                                                   {
                                                                       {
                                                                           typeof(TodoMvcHomePage),
                                                                           "http://todomvc.com/"
                                                                       },
                                                                       {
                                                                           typeof(AngularJsPage),
                                                                           "http://todomvc.com/examples/angularjs/#/"
                                                                       },
                                                                       {
                                                                           typeof(ReactPage),
                                                                           "http://todomvc.com/examples/react/#/"
                                                                       }
                                                                   };
        /// <summary>
        /// Navigation Manager Name
        /// </summary>
        public override string Name => "NavigationManager";

        /// <summary>
        /// Navigate To The Page
        /// </summary>
        /// <typeparam name="T">page object</typeparam>
        /// <returns>new page object</returns>
        public T NavigateToThePage<T>() where T : IBasePage
        {
            var urlToNavigate = this.pageUrlMap[typeof(T)];
            BrowserPool.CurrentBrowser.Instance.Navigate().GoToUrl(urlToNavigate);
            return Activator.CreateInstance<T>();
        }
    }
}