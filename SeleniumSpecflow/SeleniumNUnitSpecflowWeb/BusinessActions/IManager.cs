﻿namespace SeleniumNUnitSpecflowWeb.BusinessActions
{
    /// <summary>
    /// Manager Interface
    /// </summary>
    public interface IManager
    {
        /// <summary>
        /// Get Manager Name
        /// </summary>
        string Name { get; }
    }
}
